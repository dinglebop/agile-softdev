defmodule Yahtzee do
  def score_upper(dice) do
    %{
      Ones: _die_score(dice, 1),
      Twos: _die_score(dice, 2),
      Threes: _die_score(dice, 3),
      Fours: _die_score(dice, 4),
      Fives: _die_score(dice, 5),
      Sixes: _die_score(dice, 6)
    }
  end

  def _die_score(dice, die) do
    length(Enum.filter(dice, fn e -> e == die end))
  end

  def score_lower(dice) do
    occurrences = _count_occurrences(dice)
    %{
      "Three of a kind": _score_lower(dice, occurrences),
      "Four of a kind": _score_lower(dice, occurrences),
      "Full house": _score_lower(dice, occurrences),
      Yahtzee: _score_lower(dice, occurrences),
      "Small straight": _score_straight(dice),
      "Large straight": _score_straight(dice),
      Chance: _score_chance(dice, occurrences)
    }
  end

  def _score_lower(dice, occurrences) do
    cond do
      Enum.member?(occurrences, 3) && Enum.member?(occurrences, 2) -> 25
      Enum.member?(occurrences, 3) -> _sum_dice_score(dice)
      Enum.member?(occurrences, 4) -> _sum_dice_score(dice)
      Enum.member?(occurrences, 5) -> 50
      true -> 0
    end
  end

  def _score_straight(dice) do
    sorted_dice = Enum.sort(Enum.uniq(dice))
    cond do
      length(sorted_dice) < 4 -> 0
      (sorted_dice -- [1, 2, 3, 4]) == [] -> 30
      (sorted_dice -- [2, 3, 4, 5]) == [] -> 30
      (sorted_dice -- [3, 4, 5, 6]) == [] -> 30
      (sorted_dice -- [1, 2, 3, 4, 5]) == [] -> 40
      (sorted_dice -- [2, 3, 4, 5, 6]) == [] -> 40
      true -> 0
    end
  end

  def _score_chance(dice, occurrences) do
    cond do
      _score_lower(dice, occurrences) == 0 && _score_straight(dice) == 0 -> _sum_dice_score(dice)
      true -> 0
    end
  end

  def _count_occurrences(dice) do
    acc = %{}
    acc = dice |> Enum.reduce(%{}, fn x, acc -> Map.update(acc, x, 1, &(&1 + 1)) end)
    Map.values(acc)
  end

  def _sum_dice_score(dice) do
    Enum.sum(dice)
  end

end
