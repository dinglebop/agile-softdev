defmodule Hangman do
  def score_guess(tuple, letter) do
    word = elem(tuple, 0)
    corrects = elem(tuple, 1)
    wrongs = elem(tuple, 2)
    remaining = elem(tuple, 3)

    corrects_candidate = corrects <> letter
    wrongs_candidate = wrongs <> letter

    cond do
      remaining == 0 -> tuple
      String.contains?(wrongs, letter) -> tuple
      String.contains?(corrects, letter) -> tuple
      !String.contains?(word, letter) -> {word, corrects, wrongs_candidate, remaining - 1}
      String.contains?(word, letter) -> {word, corrects_candidate, wrongs, remaining - 1}
      true -> tuple
    end
  end

  def format_feedback(tuple) do
    word = elem(tuple, 0)

    correct_guesses = Enum.uniq(String.graphemes(elem(tuple, 1)))
    correct_letters = Enum.uniq(String.graphemes(word))
    unguessed_letters = correct_letters -- correct_guesses

    String.replace(word, unguessed_letters, "-", global: true)
  end

end
